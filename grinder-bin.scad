/* clang-format off */

/* [Bin] */
// The height of the bin, in gridfinity "units"
Bin_Height=3;//[3,6]

/* [Tool] */
// Length of the tool (mm)
Tool_Len = 125;

// Diameter of the tool (mm)
Tool_Diam = 17;

// Angle of the tool rest
Tool_Angle = 0;

// Show tool cylinder
Show_Tool = false;

/* [Bits] */

// Length of the bits (mm)
Bit_Len = 50;

// Diameter of the Bits (mm)
Bit_Diam = 3.25;

// Show the bits
Show_Bits = false;

/* [Bit Racks */

// Select this to choose the long direction for laying out the bits, if selected bit and row counts are combined for a single row of bit slots instead of multiple rows
Bit_Layout_Long = false;

// How many bits in a row
Bit_Count = 10;

// Spacing between bits (mm)
Bit_Spacing = 5;

// Number of rows of bits
Row_Count = 3;

// Anngle of the bit racks
Bit_Angle = 3;

/* [ Rendering Details ] */

// The segment length to use when rendering in preview mode
preview_segment_length=1.0;
// The minimum arc degrees per segment when rendering in preview mode
preview_arc_degrees=5.0;
// The segment length to use when rendering for export. A good value would be half your nozzle width for 3d printers
export_segment_length=0.16;
// The minimum arc degrees per segment when rendering for export. 1 will give good resolution for all the models
export_arc_degrees=1.0;

/* [Positioning Fine Tuning] */

// X Direction Offset for the tool, default is good for both long and normal layouts
Tool_X_Offset = -30;

// Y Direction Offset for the tool, default is good for both long and normal layouts
Tool_Y_Offset = -10;

// Z Direction Offset for the tool, default is good for both long and normal layouts
Tool_Z_Offset = 13.5;

// X Direction Offset for the bit racks, default is good for both long and normal layouts
Rack_X_Offset = 34;

// Y Direction Offset for the bit racks, recommend -76 for long layout, -80 for normal
Rack_Y_Offset = -80;

// Z Direction Offset for the bit racks, default is good for both long and normal layouts
Rack_Z_Offset = 17.5;


module __Customizer_Hide__ () {}// Below variables won't show in the customizer

$fs = $preview ? preview_segment_length : export_segment_length;
$fa = $preview ? preview_arc_degrees : export_arc_degrees;

/* The width of the bin */
Bin_Width = 80;

/* Radius of the tool, used for some calculations */
Tool_Rad = Tool_Diam/2;

/**
 * Maximum depth into the bin we want to cut the tool cutout.
 * This is effectively used to set the height of the tool groove top
 */
Tool_Max_Depth = 35;

/* Radius of the bits, used for some calculations */
Bit_Rad = Bit_Diam/2;

/* The width of the bit racks */
Rack_Width = Bit_Count * Bit_Spacing;

/**
 * The maximum depth we want to cut into the bin for the bits cutouts
 * This is effectively used to set the height of the bit groove top
 */
Rack_Max_Depth = Tool_Max_Depth;

/* Spacing between stacked bit racks, if modifying, be mindful of the angles */
Rack_Spacing = Bit_Len +4;

/* clang-format on */

/******************/
/** Main Render **/
/******************/
final_bin();

/**
# Final Bin

Fits all the components together in a single block for rendering the final shape
**/
module final_bin() {
  difference() {
    bin();
    tool_space();
    bit_space();
  }
}

/**
# Bin

Imports the STL file that forms the basis of the bin
**/
module bin() {
  if (Bin_Height == 3) {
    rotate([ 0, 0, 90 ]) {
      color("maroon") import("gridfinity-2x4x3.stl", convexity = 4);
    }
  }
  else {
    color("maroon") import("gridfinity-2x4x6.stl", convexity = 4);
  }
}

/**
# Tool Space

Moves the tool shape into the correct position and orientation for the
final fit with the bin.
**/
module tool_space() {
  translate([ Tool_X_Offset, Tool_Len / 2 + Tool_Y_Offset, Tool_Z_Offset ]) {
    rotate([ 0, -90 - Tool_Angle, 90 ]) { tool_groove(); }
  }
}

/**
# Tool Groove

Creates the shape that defines the groove in which the tool will sit. The top
has some extra space for getting a finger in and lifting the tool.
**/
module tool_groove() {
  hull() {

    /**
     * The upper limits of the groove. This is basically just a tall surface
     * to ensure we cut through the top of the bin
     */
    translate([ Tool_Max_Depth, 0, Tool_Len / 2 ]) {
      rotate([ 90, 0, 90 ]) { cube([ 10, Tool_Len, 1 ], center = true); }
    }

    /* The finger expansion area at the top of the groove */
    translate([ 0, 0, -Tool_Diam ]) {
      rotate([ 0, 90, 0 ]) { cylinder(h = Tool_Max_Depth + 0.5, r = Tool_Rad); }
    }

    /* The shape of the tool itself */
    tool_shape();
  }
}

/**
# Tool Shape

Creates a cylinder that represents the shape of the actual tool being stored.
This forms the basis for the rest of the tool cutout
**/
module tool_shape() {
  if (Show_Tool) {
#cylinder(h = Tool_Len, r = Tool_Rad);
  } else {
    cylinder(h = Tool_Len, r = Tool_Rad);
  }
}

/**
# Bit Space

Move the bit rack into the correct position and orientation for the
final fit with the bin
**/
module bit_space() {
  /* Pre-render to improve pre-view performance */
  render() {
    if (Bit_Layout_Long) {
      translate([ Rack_X_Offset, -Rack_Y_Offset, Rack_Z_Offset ]) {
        bit_racks();
      }
    } else {
      translate([ Rack_X_Offset, Rack_Y_Offset, Rack_Z_Offset ]) {
        bit_racks();
      }
    }
  }
}

/**
# Bit Racks

Create all of the rows of bit racks nested together
**/
module bit_racks() {

  if (Bit_Layout_Long) {
    num_bits = Bit_Count * Row_Count;
    rotate([ 90 - Bit_Angle, 0, -90 ]) {
      bit_single_rack(num_bits, Bit_Spacing);
    }
  } else {
    for (idx = [0:Row_Count - 1]) {
      translate([ 0, idx * Rack_Spacing, 0 ]) {
        /*
         * We do rotation here so each rack can be rotated independently
         * and then nested. If we rotated higher up, or in the `bit_space`
         * module the entire set of racks would be rotated in a single line
         * and instead of a set of nesting racks like this:
         *     / / /
         * We would have something more like this:
         *      /
         *     /
         *    /
         *
         */
        rotate([ 90 - Bit_Angle, 0, 180 ]) {
          bit_single_rack(Bit_Count, Bit_Spacing);
        }
      }
    }
  }
}

/**
# Bit Single Rack

Creates a single set of bit grooves in a row based on the parameters given
**/
module bit_single_rack(num_bits, spacing) {
  bit_x_offset = -spacing / 2;
  for (idx = [1:num_bits]) {
    bit_x_offset = bit_x_offset + spacing * idx;
    translate([ bit_x_offset, 0, 0 ]) { bit_groove(); }
  }
}

/**
# Bit Groove

Creates the shape that ultimately defines a single space that a bit can fit
in the finished block. The Groove is designed to fit a bit, with a bit of an
expansion area at the top for better finger access and enough space to stack
the rows of bits overlapping below each other.
**/
module bit_groove() {
  /* Defines the size of the finger expansion area at the top of the groove
   */
  expansion = min((Bit_Diam * 2), (Bit_Spacing));

  /**
   * The main Groove shape.
   * We'll subtract a small bit to ensure grooves can be nested slightly
   * beneath each other without interfearing with the grooves in front
   * of them
   */
  union() {
    hull() {

      /* Widen top of the groove */
      translate([ 0, Bit_Rad, Bit_Len - Bit_Rad ]) { sphere(d = expansion); }

      translate([ 0, expansion - Bit_Rad, 0 ]) { cylinder(d = Bit_Diam); }
      /* The shape of the actual bit */
      bit_shape();
    }

    /* The finger expansion area at the top of the groove */
    translate([ 0, -Bit_Rad, Bit_Len - expansion ]) {
      minkowski() {
        cube([ expansion + Bit_Rad, expansion, expansion * 2 ], center = true);
        sphere(Bit_Rad);
      }
    }
  }
}

/**
# Bit Shape

Creates a cylinder in the dimensions given for a bit. This forms the basis
for all of the grooves used for holding bits.
**/
module bit_shape() {
  if (Show_Bits) {
#cylinder(h = Bit_Len, d = Bit_Diam);
  } else {
    cylinder(h = Bit_Len, d = Bit_Diam);
  }
}
