# Changelog

Add a brief description of any changes you make and the date.
Order this file in reverse chronological order, and crate a
new second level heading for each new year.

## 2024

### 2024-01-01

Add tall versions of the bins (6 units)

## 2023

### 2023-12-31

After an intial test print, decided to go with a flatter bit layout due to
overhangs collapsing and difficulting laying bits in properly.

Additionally reduced the size of the bin to a proper 2 unit (per Zack's rules)
height. This necessitated taking out reducing the tool angle to 0 degrees for
the current tool diameter.

### 2023-12-30

Initial version
